<?php

echo "<h3>Menentukan Nilai</h3>";

function tentukan_nilai($number){
    $output = "";
    if($number >= 85 && $number <=100){
        $output .= "Sangat Baik";
    }else if($number >= 70 && $number < 85){
        $output .= "Baik";
    }else if($number >= 50 && $number < 70){
        $output .= "Cukup";
    }else {
        $output .= "Kurang";
    }
    return $output;
}

echo "Nilai 98 = ";
echo tentukan_nilai(98); //Sangat Baik
echo "<br> Nilai 76 = ";
echo tentukan_nilai(76); //Baik
echo "<br> Nilai 67 = ";
echo tentukan_nilai(67); //Cukup
echo "<br> Nilai 43 = ";
echo tentukan_nilai(43); //Kurang

?>